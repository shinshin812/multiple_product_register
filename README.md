# 複数ファイル同時アップロード


### 概要
product_registerで複数ファイルを一度にアップロードし、product_confirmで配列で取得しfor文を使い複数画像を表示させ、product_completeでDBに複数画像のパスを保存しました。product_confirmから戻るボタンが押されたらアップロードしたファイルが削除されます。


### ポイント
・inputタグにmultiple属性を追加
・配列で受け取り、受け取ったファイルをfor文でループさせる


### 使用技術
###### PHP 7.3.4
###### mysql 10.1.38-MariaDB
###### bootstrap 4.3.1
