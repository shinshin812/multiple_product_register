<?php
session_start();

$dsn = "mysql:dbname=test;host=localhost;charset=utf8mb4";
$user = "root";
$password = "";

try {
    $db = new PDO($dsn, $user, $password);
    echo "接続成功";
} catch (PDOException $e) {
   echo "接続失敗:" .$e->getMessage(). "\n";
   exit();
}

$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$productName = $_SESSION['product_name'];
$filename = $_SESSION['product_image']['name'];
$productPrice = $_SESSION['price'];
$productIntroduction = $_SESSION['product_introduction'];

for($i=0; $i<count($filename); $i++){
$sql="INSERT INTO product (product_name, product_image, price, product_introduction) VALUES (:productName, :filename, :productPrice, :productIntroduction)";
$stmt = $db->prepare($sql);
$stmt->bindParam(':productName',$productName, PDO::PARAM_STR);
$stmt->bindValue(':filename','img/'.$filename[$i], PDO::PARAM_STR);
$stmt->bindParam(':productPrice',$productPrice, PDO::PARAM_STR);
$stmt->bindParam(':productIntroduction',$productIntroduction, PDO::PARAM_STR);
$stmt->execute();
}
?>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
      body{
        background: #e9e9e9;
        color: #5e5e5e;
      }
      .form-wrapper {
        background: #fafafa;
        margin: 3em auto;
        padding: 20 20px;
        width: 800px;
      }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 30px 150px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
  <h2>登録完了</h2>
  <div class="form-group">
    商品名</br>
  <?php echo $_SESSION['product_name'] ?>
  </div>
  <div class="form-group">
    画像</br>
    <?php for($i=0; $i<count($_SESSION['product_image']['name']); $i++): ?>
  <img src="<?php echo "../../img/".$_SESSION['product_image']['name'][$i] ?>">
<?php endfor; ?>
  <div class="form-group">紹介文</div>
  </div>
  <?php echo $_SESSION['product_introduction'] ?>
  <div class="form-group">
    値段</br>
  <?php echo $_SESSION['price'] ?>
  </div>
  <input type="button" value="戻る" onclick="location.href='product_register.php'">
        </div>
      </div>
    </div>
  </div>
</body>
</html>
