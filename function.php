<?php
session_start();
// user_register
function displayName(){
  if(isset($_POST['name'])){
    echo $_POST['name'];
  }
}
function displayEmail(){
  if(isset($_POST['email'])){
    echo $_POST['email'];
  }
}
function displayPassword(){
  if(isset($_POST['password'])){
  echo $_POST['password'];
  }
}
function displayAddress(){
  if(isset($_POST['address'])){
    echo $_POST['address'];
  }
}
function displayCredit(){
  if(isset($_POST['credit'])){
    echo $_POST['credit'];
  }
}
function checkName(){
  if(isset($_POST['name']) && $_POST['name'] == ""){
    echo "※名前が未入力です"."</br>";
  }
}
function checkEmail(){
  if(isset($_POST['email']) && $_POST['email'] == ""){
    echo "※メールアドレスが未入力です"."</br>";
  }
}
function checkPassword(){
  if(isset($_POST['password']) && $_POST['password'] == ""){
    echo "※パスワードが未入力です"."</br>";
  }
}
function checkAddress(){
  if(isset($_POST['address']) && $_POST['address'] == ""){
    echo "※住所が未入力です"."</br>";
  }
}
function checkCredit(){
  if(isset($_POST['credit']) && $_POST['credit'] == ""){
    echo "※クレジットカード情報が未入力です";
  }
}
function userCheckOk(){
  if(isset($_POST['name'])){
    if($_POST['name'] != "" && $_POST['email'] != ""){
      if($_POST['password'] != "" && $_POST['address'] != ""){
        if($_POST['credit'] != ""){
          $_SESSION = $_POST;
          header('location: user_confirm.php');
          exit();
        }
      }
    }
  }
}

// product_register
function displayProductName(){
  if(isset($_POST['product_name'])){
    echo $_POST['product_name'];
  }
}
function displayProductImage(){
  if(isset($_POST['product_image'])){
    echo $_POST['product_image'];
  }
}
function displayProductIntroduction(){
  if(isset($_POST['product_introduction'])){
    echo $_POST['product_introduction'];
  }
}
function displayPrice(){
  if(isset($_POST['price'])){
    echo $_POST['price'];
  }
}
function checkProductName(){
  if(isset($_POST['product_name']) && $_POST['product_name'] == ""){
    echo "※商品名が未入力です"."</br>";
  }
}
function checkProductIntroduction(){
  if(isset($_POST['product_introduction']) && $_POST['product_introduction'] == ""){
    echo "※紹介文が未入力です"."</br>";
  }
}
function checkPrice(){
  if(isset($_POST['price']) && $_POST['price'] == ""){
    echo "※値段が未入力です";
  }
}
function checkOk(){
  if(isset($_POST['product_name'])){
    if($_POST['product_name'] != ""){
      if($_POST['product_introduction'] != "" && $_POST['price']){
        foreach ($_FILES['product_image']['tmp_name'] as $i => $tempfile) {
        $tempfile = $_FILES['product_image']['tmp_name'][$i];
        $filemove = '/Applications/XAMPP/htdocs/img/' . $_FILES['product_image']['name'][$i];
        move_uploaded_file($tempfile , $filemove );
        $_SESSION=$_POST;
        for($i=0; $i<count($_FILES['product_image']['name']); $i++){
        $_SESSION['product_image']['name'][$i]=$_FILES['product_image']['name'][$i];
          }
        }
        header('location: product_confirm.php');
        exit();
      }
    }
  }
}
?>
