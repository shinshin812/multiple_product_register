<?php
require('function.php');
checkOk();

if(isset($_POST['back'])){
  for($i=0; $i<count($_SESSION['product_image']['name']); $i++){
  unlink("../../img/".$_SESSION['product_image']['name'][$i]);
  }
}
 ?>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
    body{
      background: #e9e9e9;
      color: #5e5e5e;
    }
    .form-wrapper {
      background: #fafafa;
      margin: 3em auto;
      padding: 20 20px;
      width: 500px;
    }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 90px 320px;
    }
    .form-control{
      margin-bottom: 10px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
        <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
          <div class="alert-danger" role="alert"><?php echo checkProductName() ?></div>
          <div class="alert-danger" role="alert"><?php echo checkProductIntroduction() ?></div>
          <div class="alert-danger" role="alert"><?php echo checkPrice() ?></div>
<h1 class="text-info">商品登録</h1>
<form method="post" action="" enctype="multipart/form-data">
  <div class="form-group">
    商品名
  <input class="form-control" placeholder="Product Name" type="text" name="product_name" value="<?php displayProductName() ?>">
  </div>
  <div class="form-group">
    画像
  <input type="file" name="product_image[]" multiple="multiple">
  </div>
  <div class="form-group">
    紹介文
  <textarea class="form-control" placeholder="Introduction" name="product_introduction"><?php displayProductIntroduction() ?></textarea>
  </div>
  <div class="form-group">
    値段
  <input class="form-control" placeholder="Price" type="text" name="price" value="<?php displayPrice() ?>">
  </div>
  <input class="btn btn-info btn-lg" type="submit" value="確認">
</form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
