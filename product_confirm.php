<?php
session_start();

$dsn = "mysql:dbname=test;host=localhost;charset=utf8mb4";
$user = "root";
$password = "";

try {
    $db = new PDO($dsn, $user, $password);
    echo "接続成功";
} catch (PDOException $e) {
   echo "接続失敗:" .$e->getMessage(). "\n";
   exit();
}

$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

 ?>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
      body{
        background: #e9e9e9;
        color: #5e5e5e;
      }
      .form-wrapper {
        background: #fafafa;
        margin: 3em auto;
        padding: 20 20px;
        width: 800px;
      }
    .text-center{
        font-weight: bold;
        font-size: 17px;
        margin: 30px 150px;
    }
    .form-item{
      margin-bottom: 15px;
    }
</style>
<body>
  <div class="container">
    <div class="row">
      <div class="text-center">
  <form method="post" action="product_complete.php" enctype="multipart/form-data">
    <div class="form-wrapper" style="border: 1px solid #D3D3D3;">
    <div class="form-group">
      商品名</br>
    <?php echo $_SESSION['product_name'] ?>
    </div>
    <div class="form-group">
      画像</br>
      <?php for($i=0; $i<count($_SESSION['product_image']['name']); $i++): ?>
    <img src="<?php echo "../../img/".$_SESSION['product_image']['name'][$i] ?>">
  <?php endfor; ?>
    </div>
    <div class="form-group">
      紹介文</br>
    <?php echo $_SESSION['product_introduction'] ?>
    </div>
    <div class="form-group">
      値段</br>
    <?php echo $_SESSION['price'] ?>
    </div>
    <div class="form-item">
    <input class="btn btn-info btn-lg" type="submit" value="送信">
    </div>
  </form>
  <form method="post" action="product_register.php">
  <input type="submit" value="戻る" name="back">
</form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
